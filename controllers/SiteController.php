<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Noticias;



class SiteController extends Controller
{
        

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       //$modelos=Noticias::find()->asArray()->all(); Si nos lo piden en un array
         $modelos=Noticias::find()->all();
        return $this->render('index',[
                    'noticia'=>$modelos[1],
            
                ]);
    }

  
   
}

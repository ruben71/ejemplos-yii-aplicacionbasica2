<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $id
 * @property string $titulo
 * @property string $texto
 * @property string $fecha
 * @property string $foto
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','titulo'], 'required'],
            [['id'], 'integer'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 50],
            [['texto'], 'string', 'max' => 300],
            [['foto'], 'string', 'max' => 20],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo de la noticia',
            'texto' => 'Texto completo',
            'fecha' => 'Fecha de Publicación',
            'foto' => 'URL de la Foto',
        ];
    }
}
